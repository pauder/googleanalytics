<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once dirname(__FILE__).'/functions.php';

bab_functionality::includefile('SitemapEditorNodeSection');

class Func_SitemapEditorNodeSection_GoogleConversionTracking extends Func_SitemapEditorNodeSection 
{
	public function getDescription()
	{
		return ganalytics_translate('Google AdWords conversion tracking');
	}
	
	
	/**
	 * Additional method specific to conversion traking
	 * the value will be used in conversion if the value in database is null
	 * 
	 * @see gAnalytics_setConversion()
	 * 
	 * 
	 */
	public function conversionValue($value = null)
	{
		static $conversion_value = null;
		
		if (null !== $value)
		{
			$conversion_value = $value;
		}
		
		return $conversion_value;
	}
	
	
	
	/**
	 * Get displayable frame for the node editor
	 * if the method return NULL the section is not displayed
	 *
	 * @param	string	$content_type		container | url | category | topic | ref
	 * @param	string	$node				function ID of sitemap node, NULL for the edit form of a new node
	 *
	 * @return Widget_Widget
	 */
	public function getFormSection($content_type, $nodeId = null) {
		
		$W = bab_Widgets();
		$frame = $W->Frame(null, $W->VBoxLayout()->setVerticalSpacing(1,'em'));
		
		$languages = $W->Select()
			->addOption('fr', ganalytics_translate('French'))
			->addOption('en', ganalytics_translate('English'));
		
		$frame->addItem($this->textfield(ganalytics_translate('ID'), 'conversion_id'));
		$frame->addItem($this->labelledField(ganalytics_translate('Language'), $languages, 'conversion_language'));
		$frame->addItem($this->textfield(ganalytics_translate('Format'), 'conversion_format'));
		$frame->addItem($this->labelledField(ganalytics_translate('Color'), $W->ColorPicker(), 'conversion_color'));
		$frame->addItem($this->textfield(ganalytics_translate('Label'), 'conversion_label'));
		$frame->addItem($this->textfield(ganalytics_translate('Value'), 'conversion_value'));
		
		return $frame;
	}
	
	
	/**
	 * 
	 * @param string $label
	 * @param string $name
	 * 
	 * @return smed_LabelledWidget
	 */
	protected function textfield($label, $name)
	{
		$W = bab_Widgets();
		return $this->labelledField($label, $W->LineEdit(), $name);
	}
	
	
	/**
	 * Get recorded values for the node Id
	 * @param string $nodeId
	 * @return array | null
	 */
	public function getValues($nodeId)
	{
		global $babDB;
		$res = $babDB->db_query('SELECT * FROM ganalytics_conversion WHERE id_function='.$babDB->quote($nodeId));
		
		if (0 === $babDB->db_num_rows($res))
		{
			return null;
		}
		
		$arr = $babDB->db_fetch_assoc($res);
		
		if (!$arr)
		{
			return null;
		}
		
		return $arr;
	}
	
	
	/**
	 * Save information posted by the form section
	 *
	 * @param	string	$content_type		container | url | category | topic | ref
	 * @param	string	$node				function ID of sitemap node
	 * @param	array	$values				posted values of this form section
	 */
	public function saveFormSection($content_type, $nodeId, $values = array()) {
		global $babDB;
		
		if ('' === $values['conversion_id'] || '' === $values['conversion_label'])
		{
			$babDB->db_query("DELETE FROM ganalytics_conversion WHERE id_function=".$babDB->quote($nodeId));	
			return;
		}
		
		
		if ('' === $values['conversion_value'])
		{
			$values['conversion_value'] = NULL;
		}
		
		$currentValues = $this->getValues($nodeId);
		
		if (null === $currentValues)
		{
			
			$babDB->db_query("INSERT INTO ganalytics_conversion 
					(id_function, 
						conversion_id,
						conversion_language,
						conversion_format,
						conversion_color,
						conversion_label,
						conversion_value
					) 
				VALUES 
					(".$babDB->quote($nodeId).",
						".$babDB->quote($values['conversion_id']).",
						".$babDB->quote($values['conversion_language']).",
						".$babDB->quote($values['conversion_format']).",
						".$babDB->quote($values['conversion_color']).",
						".$babDB->quote($values['conversion_label']).",
						".$babDB->quoteOrNull($values['conversion_value'])."
					)
			");
			
		} else {
			
			
			$babDB->db_query("UPDATE ganalytics_conversion 
					SET 
						conversion_id=".$babDB->quote($values['conversion_id']).",
						conversion_language=".$babDB->quote($values['conversion_language']).",
						conversion_format=".$babDB->quote($values['conversion_format']).",
						conversion_color=".$babDB->quote($values['conversion_color']).",
						conversion_label=".$babDB->quote($values['conversion_label']).",
						conversion_value=".$babDB->quoteOrNull($values['conversion_value'])."
					WHERE id_function=".$babDB->quote($nodeId)
			);
		}
		
		
	}
}