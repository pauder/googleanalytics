; <?php/*
[general]
name							="googleanalytics"
version							="0.1.7"
encoding						="UTF-8"
mysql_character_set_database	="latin1,utf8"
description						="Google analytics and AdWords conversion traking"
description.fr					="Module d'intégration avec Google Analytics et AdWords"
long_description.fr             ="README.md"
delete							=1
addon_type						="EXTENSION"
ov_version						="8.1.98"
php_version						="5.2.0"
addon_access_control			=0
author							="Cantico"
icon							="icon.png"
configuration_page				="admin" 
tags							="extension,recommend,website"

[addons]

sitemap_editor="0.5.6"

;*/?>