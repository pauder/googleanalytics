<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once dirname(__FILE__).'/functions.php';


class Func_GoogleAnalytics extends bab_functionality 
{
	
	private $_gaq = array();
	
	private $dimensions = array();
	
	
	public function getDescription()
	{
		return ganalytics_translate('Manipulate Google Analytics query');
	}
	
	
	public function _setAccount($accountID)
	{
		array_unshift($this->_gaq, array(__FUNCTION__, $accountID));
	}
	
	
	/**
	 * Set dimention property
	 * Dimentions must be created on ga back office for each used indexes
	 * 
	 * @param int $index
	 * @param string $value
	 */
	public function setDimention($index, $value)
	{

	    $this->dimensions['dimension'.$index] = $value;
	}
	
	/**
	 * Add additional parameters to the _gaq javascript array
	 * 
	 * @param	string	$method
	 * @param	mixed	$parameters
	 * 
	 */
	public function __call($method, $parameters = null)
	{
		$append = array($method);
		if (null !== $parameters && !empty($parameters))
		{
			$append = array_merge($append, $parameters);
		}
		
		$this->_gaq[] = $append;
	}
	
	/**
	 * Get Analytics javascript
	 */
	public function getJavascript()
	{		
		$javascript = "var _gaq = _gaq || [];
		";
		
		if (count($this->dimensions) > 0) {
		    $this->_gaq[] = array('dimensions', $this->dimensions);
		}

		// must use push method on the _gaq object, _gaq can be an object or an array if ga.js is not loaded
		
		foreach($this->_gaq as $arr)
		{
			$javascript .= "
				 _gaq.push(".ganalytics_json_encode($arr).");";
		}
		
		
		
		// the _gaq variable is now processed by the Tag_google_analytics.js code

		
		return $javascript;
	}
	
}